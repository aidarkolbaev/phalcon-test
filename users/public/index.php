<?php

use App\Controllers\JsonRPCController;
use App\Middleware\ValidationMiddleware;
use Phalcon\Di\FactoryDefault;
use Phalcon\Events\Manager;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

error_reporting(E_ALL);

define('API_BASE_PATH', dirname(__DIR__));
define('API_PATH', API_BASE_PATH . '/app');

try {

    $di = new FactoryDefault();

    include API_PATH . '/config/services.php';

    include API_PATH . '/config/loader.php';

    $eventsManager = new Manager();
    $app = new Micro($di);

    $eventsManager->attach('micro', new ValidationMiddleware());
    $app->before(new ValidationMiddleware());

    $app->setEventsManager($eventsManager);

    $collection = new MicroCollection();
    $collection->setHandler(new JsonRPCController());
    $collection->post('/', 'handle');
    $app->mount($collection);

    $app->notFound(function () use ($app) {
       return $app->response->setJsonContent(['message' => 'Error'])->setStatusCode(409)->send();
    });

    $app->handle();

} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}