<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller;

abstract class AbstractJsonRPCController extends Controller
{
    public static function toJsonRPC($data, $id)
    {
        if ($data['status']) {
            $response = [
                'jsonrpc' => '2.0',
                'result' => $data['data'],
                'id' => $id
            ];
        } else {
            $response = [
                'jsonrpc' => '2.0',
                'error' => [
                    'code' => $data['code'],
                    'message' => $data['message']
                ],
                'id' => $id
            ];
        }
        return $response;
    }

    public static function error($message, $code)
    {
        return [
            'status' => false,
            'message' => $message,
            'code' => is_numeric($code) ? $code : 666
        ];
    }

    public static function success($data)
    {
        return [
            'status' => true,
            'data' => $data
        ];
    }
}