<?php

namespace App\Controllers;


use App\Models\User;

class JsonRPCController extends AbstractJsonRPCController
{
    public function handle()
    {
        $request = $this->request->getJsonRawBody(true);
        $method = $request['method'];
        $response = call_user_func_array([self::class, $method], $request['params']);
        if (!$response) {
            $response = self::error('метод не найден', 409);
        }
        return $this->response->setJsonContent(self::toJsonRPC($response, $request['id']))->send();
    }

    public static function authorize($username, $password)
    {
        $user = User::findByUsernameAndPassword($username, $password);
        if (!$user) {
            return self::error('неверный логин или пароль', 404);
        }
        return self::success(['message' => 'успешная авторизация']);
    }
}