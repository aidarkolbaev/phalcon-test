<?php

namespace App\Middleware;

use App\Controllers\JsonRPCController;
use Phalcon\Events\Event;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

class ValidationMiddleware implements MiddlewareInterface
{

    /**
     *
     * @param Event $event
     * @param Micro $application
     *
     * @return bool
     */
    public function beforeHandleRoute(Event $event, Micro $application)
    {
        $request = $application->request->getJsonRawBody(true);
        if (!$request || empty($request['jsonrpc']) || empty($request['method']) ||
            empty($request['params']) || empty($request['id'])) {
            $response = JsonRPCController::toJsonRPC(
                JsonRPCController::error('неверный запрос', 400),
                !empty($request['id']) ? $request['id'] : 0
            );
            $application->response->setJsonContent($response)->send();
            return false;
        }
        return true;
    }

    /**
     * Calls the middleware
     *
     * @param \Phalcon\Mvc\Micro $application
     * @return bool
     */
    public function call(Micro $application)
    {
        return true;
    }
}