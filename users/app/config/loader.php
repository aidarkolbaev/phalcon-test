<?php

$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    [
        'App\Models' => $di->get('config')->application->modelsDir,
        'App\Middleware' => $di->get('config')->application->middlewareDir,
        'App\Controllers' => $di->get('config')->application->controllersDir
    ]
)->register();