<?php

use Phalcon\Db\Adapter\Pdo\Sqlite;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include API_PATH . "/config/config.php";
});

$di->setShared('db', function () {
    $config = $this->getConfig();
    return new Sqlite((array)$config->database);
});