<?php

defined('API_BASE_PATH') || define('API_BASE_PATH', getenv('API_BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('API_PATH') || define('API_PATH', API_BASE_PATH . '/app');

return new \Phalcon\Config([
    'database' => [
        'dbname' => '/var/www/users/public/db/users.sqlite'
    ],
    'application' => [
        'modelsDir' => API_PATH . '/models/',
        'middlewareDir' => API_PATH . '/middleware/',
        'controllersDir' => API_PATH . '/controllers/'
    ]
]);