<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class User extends Model
{
    /** @var int */
    private $rowid;

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /**
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->rowid;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("users");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'users';
    }


    public static function findByUsernameAndPassword($username, $password) {
        return self::findFirst(
            [
                'conditions' => 'username = :username: AND password = :password:',
                'bind' => [
                    'username' => $username,
                    'password' => $password
                ]
            ]
        );
    }
}