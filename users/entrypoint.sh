#!/bin/bash

service php7.2-fpm start
sqlite3 /var/www/users/public/db/users.sqlite < '/var/www/users/app/migrations/1.0.0/1.0.0.sql'
nginx -g "daemon off;"