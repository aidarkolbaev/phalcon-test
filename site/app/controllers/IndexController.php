<?php

namespace App\Controllers;

use App\Service\JsonRPCClient;
use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
    public function indexAction() {
        $message = $this->request->getQuery('message');
        return $this->view->setVars(['message' => $message])->render('index', 'index');
    }

    public function loginAction() {
        $data = [
            'username' => $this->request->getPost('username'),
            'password' => $this->request->getPost('password')
        ];

        if (!empty($data['username']) && !empty($data['password'])) {
            $client = new JsonRPCClient($this->config->application->apiUrl, 1);
            $response = $client->request('authorize', $data);

            if ((!empty($response['error']) && empty($response['result']))) {
                $message = $response['error']['message'];
            } else if ($response && !empty($response['result'])) {
                $message = $response['result']['message'];
            } else {
                $message = 'произошла ошибка';
            }
        } else {
            $message = 'Невалидные данные';
        }

        return $this->response->redirect('/?message='.$message);
    }
}