<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    [
        'App\Controllers' => $di->get('config')->application->controllersDir,
        'App\Service' => $di->get('config')->application->serviceDir
    ]
)->register();
