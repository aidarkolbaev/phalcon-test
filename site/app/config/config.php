<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

return new \Phalcon\Config([
    'application' => [
        'controllersDir' => APP_PATH . '/controllers/',
        'viewsDir'       => APP_PATH . '/views/',
        'serviceDir'     => APP_PATH . '/service/',
        'apiUrl'         => getenv('API_URL') ?: 'http://api:8080',
        'baseUri'        => '/',
    ]
]);
