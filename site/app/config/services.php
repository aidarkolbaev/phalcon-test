<?php

use Phalcon\Mvc\Url;
use Phalcon\Mvc\View\Simple as View;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});


$di->setShared('url', function () {
    $config = $this->getConfig();
    $url = new Url();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Sets the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    return $view;
});

