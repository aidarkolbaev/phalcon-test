<?php

use Phalcon\Mvc\Micro\Collection;


$collection = new Collection();
$collection->setHandler('\App\Controllers\IndexController', true);
$collection->get('/', 'indexAction');
$collection->post('/login', 'loginAction');
$app->mount($collection);


$app->notFound(function () use ($app) {
    return $app->response->setContent("<h1>Not Found</h1>")->setStatusCode(404)->send();
});