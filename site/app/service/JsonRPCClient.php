<?php

namespace App\Service;


class JsonRPCClient
{
    /** @var string */
    private $url;

    /** @var int */
    private $id;

    public function __construct($url, $id)
    {
        $this->url = $url;
        $this->id = $id;
    }

    public function request($method, $params) {
        $request = [
            'jsonrpc' => '2.0',
            'method' => $method,
            'params' => $params,
            'id' => $this->id
        ];
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));

        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response, true);
    }
}